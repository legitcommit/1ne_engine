include .env

CFLAGS = -std=c++17 -I. -I$(VULKAN_INCLUDE)
LDFLAGS = -lglfw -lvulkan -ldl -lpthread -lX11 -lXxf86vm -lXrandr -lXi

OUTPUT_FILE = a.out

$(OUTPUT_FILE): *.cpp *.h
	g++ -Wall -g $(CFLAGS) -o $(OUTPUT_FILE) *.cpp $(LDFLAGS)

.PHONY: test clean

test: $(OUTPUT_FILE)
	./$(OUTPUT_FILE)

clean:
	rm -f $(OUTPUT_FILE)