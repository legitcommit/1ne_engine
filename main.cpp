#include "app_test.h"
#include <cstdlib>
#include <exception>
#include <iostream>

int main()
{
    _1neEngine::AppTest app{};

    try
    {
        app.run();
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << '\n';
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}