#pragma once

#include "1ne_window.h"
#include <GL/glext.h>

namespace _1neEngine
{
    class AppTest
    {
        public:

        static constexpr int WIDTH = 800;
        static constexpr int HEIGHT = 600;

        void run();

        private:

        _1EWindow _1eWindow{WIDTH, HEIGHT, "window"};
    };
}