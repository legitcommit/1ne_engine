#include "1ne_window.h"
#include <GLFW/glfw3.h>
#include <string>

namespace _1neEngine
{
    _1EWindow::_1EWindow(int w, int h, std::string name) : width{w}, height{h}, name{name}
    {
        initWindow();
    }

    _1EWindow::~_1EWindow()
    {
        glfwDestroyWindow(window);
        glfwTerminate();
    }

    void _1EWindow::initWindow()
    {
        glfwInit();
        glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
        glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

        window = glfwCreateWindow(width, height, name.c_str(), nullptr, nullptr);
    }
}