#pragma once

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <string>
#include <vulkan/vulkan_core.h>

namespace _1neEngine
{
    class _1EWindow
    {
        public:

        _1EWindow(int width, int height, std::string name);
        ~_1EWindow();

        bool shouldClose()
        {
            return glfwWindowShouldClose(window);
        }

        private:

        void initWindow();
        const int width;
        const int height;

        std::string name;
        GLFWwindow *window;
    };
}